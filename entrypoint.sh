#!/bin/sh

# output errors
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# run nginx in the foreground
nginx -g 'daemon off;'
